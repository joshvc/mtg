1 Ash Barrens
1 Caves of Koilos
1 Command Tower
1 Dragonskull Summit
1 Evolving Wilds
1 Exotic Orchard
5 Forest
1 Grand Coliseum
1 Gruul Turf
1 Jungle Shrine
1 Karplusan Forest
1 Mosswort Bridge
5 Mountain
1 Nomad Outpost
1 Orzhov Basilica
3 Plains
1 Rootbound Crag
1 Sandsteppe Citadel
1 Savage Lands
1 Spinerock Knoll
1 Sunpetal Grove
3 Swamp
1 Terramorphic Expanse
1 Windbrisk Heights
1 Ajani Unyielding
1 Alesha, Who Smiles at Death
1 Angel of Invention
1 Ankle Shanker
1 Artifact Mutation
1 Aura Mutation
1 Beastmaster Ascension
1 Blind Obedience
1 Blossoming Defense
1 Boros Charm
1 Brutal Hordechief
1 Charging Cinderhorn
1 Clan Defiance
1 Commander's Sphere
1 Conqueror's Flail
1 Crackling Doom
1 Dauntless Escort
1 Diabolic Tutor
1 Domri Rade
1 Farseek
1 Fellwar Stone
1 Fog
1 Fumigate
1 Gisela, Blade of Goldnight
1 Grave Upheaval
1 Gruul Signet
1 Hanweir Militia Captain
1 Iroas, God of Victory
1 Kambal, Consul of Allocation
1 Lifecrafter's Bestiary
1 Lightning Greaves
1 Managorger Hydra
1 Mirror Entity
1 Mycoloth
1 Naya Charm
1 Oath of Ajani
1 Odric, Lunarch Marshal
1 Order // Chaos
1 Primeval Protector
1 Rampant Growth
1 Ravos, Soultender
1 Ready // Willing
1 Release the Gremlins
1 Reverent Mantra
1 Sakura-Tribe Elder
1 Saskia the Unyielding
1 Skullclamp
1 Sol Ring
1 Stalking Vengeance
1 Stand or Fall
1 Stasis Snare
1 Stonehoof Chieftain
1 Sunforger
1 Swords to Plowshares
1 Sylvan Reclamation
1 Taurean Mauler
1 Terminate
1 Thalia's Lancers
1 Thunderfoot Baloth
1 Utter End
1 Verdurous Gearhulk
1 Wild Beastmaster
1 Wilderness Elemental
1 Zhur-Taa Druid
Sideboard:

1 Abzan Charm
1 Acrobatic Maneuver
1 Authority of the Consuls
1 Breath of Fury
1 Bristling Hydra
1 Cryptolith Rite
1 Den Protector
1 Divergent Transformations
1 Everlasting Torment
1 Evolutionary Escalation
1 Felidar Guardian
1 Frenzied Fugue
1 Gonti's Aether Heart
1 Grab the Reins
1 Incendiary Flow
1 Korozda Guildmage
1 Lavalanche
1 Lightning Runner
1 Marionette Master
1 Mentor of the Meek
1 Necrogenesis
1 Ninth Bridge Patrol
1 Oviya Pashiri, Sage Lifecrafter
1 Quirion Explorer
1 Repel the Abominable
1 Ridgescale Tusker
1 Rishkar's Expertise
1 Selesnya Guildmage
1 Shamanic Revelation
1 Sorin's Vengeance
1 Soul of the Harvest
1 Sylvok Explorer
1 Tana, the Bloodsower
1 Thelonite Hermit
1 Thopter Arrest
1 Thraben Inspector
1 Treacherous Terrain
1 Tymna the Weaver
1 Wight of Precinct Six
1 Winding Constrictor
1 Yahenni's Expertise